/*
 * Copyright 2008 Jacek Caban
 * Copyright 2008 Piotr Caban
 * Copyright 2008 Jaroslaw Sobiecki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ui.h"
#include <assert.h>
#include <exception>
#include <string>
#include <iostream>
#include <sstream>

//forward declaration

/**
 *  This is constructor of UI class
 */
UI::UI(Ogre::Root *&root, 
       CEGUI::OgreCEGUIRenderer *renderer, 
       CEGUI::System *system, 
       CEGUI::WindowManager *wmgr, 
       bool *cont,
       PoolGame *game) :
  mRoot(root),
  mRenderer(renderer),
  mSystem(system),
  mWindowManager(wmgr),
  mContinue(cont),
  mGame(game)
{
  main_sheet = mWindowManager->loadWindowLayout((CEGUI::utf8*) "main_menu.layout");
  hud_sheet = mWindowManager->loadWindowLayout((CEGUI::utf8*) "game_hud.layout");
  in_game_sheet = mWindowManager->loadWindowLayout((CEGUI::utf8*) "game_menu.layout");
  mPowerShoot = 0.5f;

};


/**
 * This method returns current value of slider which represents power of
 * shoot. It's real value betwen 0 and 1.
 */
Ogre::Real UI::getPowerShot()
{
  return mPowerShoot;
}

/**
 * This method switch current menu
 */
bool UI::setScene (Scene sc) {
  CEGUI::Window *tmp_window = NULL;
  switch (sc)
  {
    case M_MAIN_MENU:
      mSystem->setGUISheet(main_sheet);
      break;
    case M_DISABLED:
      setScene(M_HUD_MENU);
      break;
    case M_IN_GAME_MENU:
      mSystem->setGUISheet(in_game_sheet);
      break;
    case M_HUD_MENU:
      mSystem->setGUISheet(hud_sheet);

      //tmp_window = mWindowManager->getWindow("HUD/PowerSlider");
      //tmp_window->deactivate();
      break;
    case M_SET_SHOT_POWER:
      //THIS STATE IS UNUSED. 
      //tmp_window = mWindowManager->getWindow("HUD/PowerSlider");
      //tmp_window->deactivate();
      break;
    default:
      //throw Exception("Unknown scene at UI::setScene method");
      break;
  }

  setEventHandlers(sc);
  this->mCurrentScene = sc;
  return true;
}

/**
 *  This method returns currently choosen menu
 */
Scene UI::getScene() {
    return mCurrentScene;
}

/**
 *  This is handler of click event. It simply switch from menu to game
 */
bool UI::continueButtonHandler(const CEGUI::EventArgs &event)
{
  setScene(M_DISABLED);
  return true;
}

/**
 * This is handler of click event. It resets state of game in order
 * to restart our gameplay.
 */
bool UI::restartButtonHandler(const CEGUI::EventArgs &event)
{
  mGame->restart();
  setScene(M_MAIN_MENU);
  return true;
}

/**
 * This is handler of click event. It simply quits our game
 */
bool UI::quitButtonHandler(const CEGUI::EventArgs &event)
{
  *mContinue = false;
  return true;
}

/**
 *  This is handler of click event. It's switch from main menu
 *  to game
 */
bool UI::startButtonHandler(const CEGUI::EventArgs &event)
{
  setScene(M_DISABLED);
  return true;
}

/**
 * This is handler of slider value change. It assign current
 * value of slider to member mPowerShoot
 */
bool UI::sliderButtonHandler(const CEGUI::EventArgs &event)
{
  const CEGUI::WindowEventArgs *arg = dynamic_cast<const CEGUI::WindowEventArgs*> (&event); 
  CEGUI::Slider *slider = (CEGUI::Slider*) arg->window;
  mPowerShoot = slider->getCurrentValue();
  return true;
}


/**
 * This method assign event handler to CEGUI event, connected with
 * widget (window) named window_name. This method
 * assign event of value changing to sliderButtonHandler handler
 */
void UI::add_slider_event_hander(const CEGUI::String window_name)
{
  mWindowManager->getWindow(window_name)->subscribeEvent(CEGUI::Slider::EventValueChanged,
                                                         CEGUI::Event::Subscriber(&UI::sliderButtonHandler, this));
}

/**
 * This method assign event handler to CEGUI event, connected with
 * widget (window) named window_name. This method
 * assign event of button click to startButtonHandler handler
 */
void UI::add_start_event_handler(const CEGUI::String window_name)
{
  mWindowManager->getWindow(window_name)->subscribeEvent(CEGUI::Window::EventMouseClick,
                                                         CEGUI::Event::Subscriber(&UI::startButtonHandler, this));

}

/**
 * This method assign event handler to CEGUI event, connected with
 * widget (window) named window_name. This method
 * assign event of button click to restartButtonHandler handler
 */
void UI::add_restart_event_handler(const CEGUI::String window_name)
{
  mWindowManager->getWindow(window_name)->subscribeEvent(CEGUI::Window::EventMouseClick,
                                                         CEGUI::Event::Subscriber(&UI::restartButtonHandler, this));
}

/**
 * This method assign event handler to CEGUI event, connected with
 * widget (window) named window_name. This method
 * assign event of button click to continueButtonHandler handler
 */
void UI::add_continue_event_handler(const CEGUI::String window_name)
{
  mWindowManager->getWindow(window_name)->subscribeEvent(CEGUI::Window::EventMouseClick,
                                                         CEGUI::Event::Subscriber(&UI::continueButtonHandler, this));
}


/**
 * This method assign various event handlers to 
 * various windows. It's based on currently chossen scene
 */
void UI::setEventHandlers (Scene sc)
{
  switch (sc)
  {
    case M_MAIN_MENU:
      add_quit_event_handler ("MainMenu/QuitButton");
      add_start_event_handler ("MainMenu/StartButton");
      break;
    case M_IN_GAME_MENU:
      add_quit_event_handler ("GameMenu/QuitButton");
      add_restart_event_handler("GameMenu/RestartButton");
      add_continue_event_handler("GameMenu/ContinueButton");
    case M_DISABLED:
      break;
    case M_HUD_MENU:
      add_slider_event_hander("HUD/PowerSlider");

      break;
    case M_SET_SHOT_POWER:
      break;
    default:
      break;
  }


};

/**
 * This method assign event handler to CEGUI event, connected with
 * widget (window) named window_name. This method
 * assign event of button click to quitButtonHandler handler
 */
void UI::add_quit_event_handler (const CEGUI::String window_name)
{
  mWindowManager->getWindow(window_name)->subscribeEvent(CEGUI::Window::EventMouseClick,
                                                         CEGUI::Event::Subscriber(&UI::quitButtonHandler, this));
  
}


/**
 * This is MenuFrameListener constructor
 */
MenuFrameListener::MenuFrameListener(OIS::Keyboard *keyboard, 
                                     OIS::Mouse *mouse, 
                                     Ogre::SceneManager *mgr,
                                     CEGUI::System *system,
                                     bool *cont,
                                     PoolGame *game) : 
                                     mMouse(mouse),
                                     mKeyboard(keyboard),
                                     mSystem(system),
                                     mContinue(cont),
                                     mGame(game)
{
  assert(mouse != NULL);
  assert(game != NULL);


  mouse->setEventCallback(this);
  last_player = -2;
  last_color = -2;
}

/**
 * This is implementation of FrameListener::frameStarted method.
 * It's check state of game from mGame member and set correct messages to 
 * 2d HUD
 */
bool MenuFrameListener::frameStarted(const Ogre::FrameEvent &evt)
{
  std::ostringstream osbuf (std::ostringstream::out);
  bool change = false;

  last_player = mGame->currentPlayer();
  last_color = mGame->currentColor();

  if (last_player != -1 && mGame->gameEnded() == -1)
  {
    osbuf << "CURRENT PLAYER: " << (last_player + 1);

    if (last_color != 2)
    {
      osbuf << " " << "CURRENT BALL: " << (last_color == 1 ? "STRIPES" : "SOLID");
    }
    change = true;
  }

  if (mGame->gameEnded() == -1 && mGame->isWhiteBallMovable())
  {
    osbuf << " FAUL. PLEASE PLACE WHITE BALL.";
    change = true;
  }

  if (mGame->gameEnded() != -1)
  {
    osbuf << "PLAYER " << mGame->gameEnded() + 1 << " WON";
    change = true; 
  }

   
  if (change)
  {   
    CEGUI::Window *window = CEGUI::WindowManager::getSingletonPtr()->getWindow("HUD/CurrentPlayerBallLabel");
    window->setProperty("Text", osbuf.str());
  }

  if (mMouse) mMouse->capture();
  if (mKeyboard) mKeyboard->capture();
  return *mContinue;
}

/**
 *  This is simple implementation of MouseListener::mouseMoved method. It's
 *  translates OIS mouse move event to CEGUI mouse move event
 */
bool MenuFrameListener::mouseMoved(const OIS::MouseEvent &arg)
{
  CEGUI::System::getSingleton().injectMousePosition(arg.state.X.abs*2, arg.state.Y.abs*2);
  return true;
}

/**
 *  This method translate OIS mousebutton id to CEGUI mousebutton id
 *  This code was taken from 
 *  http://www.ogre3d.org/wiki/index.php/Basic_Tutorial_7#Converting_and_Injecting_Mouse_Events
 */
CEGUI::MouseButton MenuFrameListener::convertButton(OIS::MouseButtonID buttonID)
{
    switch (buttonID)
    {
    case OIS::MB_Left:
        return CEGUI::LeftButton;

    case OIS::MB_Right:
        return CEGUI::RightButton;

    case OIS::MB_Middle:
        return CEGUI::MiddleButton;

    default:
        return CEGUI::LeftButton;
    }
}


/**
 *  This method is implementation of MouseListener::mousePressed
 *  It's translates mousePressed OIS event to CEGUI event
 */
bool MenuFrameListener::mousePressed(const OIS::MouseEvent &arg,
                                     OIS::MouseButtonID id)
{
  CEGUI::System::getSingleton().injectMouseButtonDown(convertButton(id));
  return true;
}

/**
 *  This method is implementation of MouseListener::mousePressed
 *  It's translates mouseRelased OIS event to CEGUI event
 */
bool MenuFrameListener::mouseReleased(const OIS::MouseEvent &arg,
                                      OIS::MouseButtonID id)
{
  CEGUI::System::getSingleton().injectMouseButtonUp(convertButton(id));
  return true;
}

