/*
 * Copyright 2008 Jacek Caban
 * Copyright 2008 Piotr Caban
 * Copyright 2008 Jaroslaw Sobiecki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "rules.h"

using namespace Ogre;

/**
 * Constructor of PoolGame class. It defines starting game state.
 */
PoolGame::PoolGame(Real sizeX, Real sizeY) : sizeX(sizeX), sizeY(sizeY) {
    init();
}

/**
 * Destructor of PoolGame class.
 */
PoolGame::~PoolGame() {
    destroy();
}

/**
 * This method initializes the pool game. It is used by constructor and restart method.
 */
void PoolGame::init(void) {
    game = new Table(sizeX, sizeY);

    //White ball
    game->balls.push_back(new Ball(0, Vector3(16.0f, 0.0f, 0.6f), Vector3(0.0f, 0.0f, 0.0f), Vector3(0.0f, 0.0f, 0.0f), 1.0f));

    //1-15
    game->balls.push_back(new Ball(1, Vector3(-16.0f-2*1.7320f, 2.0f, 0.6f)));
    game->balls.push_back(new Ball(2, Vector3(-16.0f-4*1.7320f, -2.0f, 0.6f)));
    game->balls.push_back(new Ball(3, Vector3(-16.0f-3*1.7320f, 1.0f, 0.6f)));
    game->balls.push_back(new Ball(4, Vector3(-16.0f-4*1.7320f, 2.0f, 0.6f)));
    game->balls.push_back(new Ball(5, Vector3(-16.0f-4*1.7320f, 4.0f, 0.6f)));
    game->balls.push_back(new Ball(6, Vector3(-16.0f-3*1.7320f, -3.0f, 0.6f)));
    game->balls.push_back(new Ball(7, Vector3(-16.0f-1.7320f, -1.0f, 0.6f)));
    game->balls.push_back(new Ball(8, Vector3(-16.0f-2*1.7320f, 0.0f, 0.6f)));
    game->balls.push_back(new Ball(9, Vector3(-16.0f, 0.0f, 0.6f)));
    game->balls.push_back(new Ball(10, Vector3(-16.0f-3*1.7320f, -1.0f, 0.6f)));
    game->balls.push_back(new Ball(11, Vector3(-16.0f-4*1.7320f, -4.0f, 0.6f)));
    game->balls.push_back(new Ball(12, Vector3(-16.0f-1.7320f, 1.0f, 0.6f)));
    game->balls.push_back(new Ball(13, Vector3(-16.0f-4*1.7320f, 0.0f, 0.6f)));
    game->balls.push_back(new Ball(14, Vector3(-16.0f-3*1.7320f, 3.0f, 0.6f)));
    game->balls.push_back(new Ball(15, Vector3(-16.0f-2*1.7320f, -2.0f, 0.6f)));

    stopped = false;
    player = 0;
    color = 2;
    faul = 0;
    memset(pocketedBalls, 0, sizeof(pocketedBalls));
}

/**
 * This method destroys game object. It frees the memory.
 */
void PoolGame::destroy(void) {
    std::vector<Ball*>::iterator iter;
    for(iter=game->balls.begin(); iter!=game->balls.end(); iter++) delete *iter;
    delete game;
}

/**
 * Restarts the game.
 */
void PoolGame::restart(void) {
    destroy();
    init();
}

/**
 * This function checks if it's time to make a shot. Returns true if so.
 */
bool PoolGame::isWaitingForShot(Ogre::Real time) {
    if(gameEnded()!=-1) return false;
    if(!stopped) {
        if(!game->update(time)) {
            bool forceChangingPlayer = false;
            bool keepPlayer = false;

            for(int i=1; i<16; i++) {
                if(pocketedBalls[i]) continue;
                if(!game->balls[i]->isOnTable()) {
                    pocketedBalls[i] = true;
                    if((color==0 && i<8) || (color==1 && i>8) || color==2) keepPlayer = true;
                    if((color==1 && i<8) || (color==0 && i>8)) forceChangingPlayer = true;
                }
            }
            stopped = true;
            if(isWhiteBallMovable() || !keepPlayer || forceChangingPlayer) {
                player = !player;
                if(color!=2) color = !color;
            }
        }
        return false;
    }
    return true;
}

/**
 * This method checks if there was a faul. If so the white ball will be movable in the game.
 */
bool PoolGame::isWhiteBallMovable(void) {
    if(!stopped) return false;
    if(faul!=2) return faul;

    int firstHit = game->balls[0]->getFirstHit();

    faul = 0;
    if(!game->balls[0]->isOnTable());
    else if(!firstHit);
    else if(color == 2) {
        int i, j;
        for(i=1; i<8; i++) if(!game->balls[i]->isOnTable()) break; 
        for(j=9; j<16; j++) if(!game->balls[j]->isOnTable()) break; 
        if(i<8 && j<16) return false;
        if(i<8) color = 0;
        if(j<16) color = 1;
        faul = 0;
        return false;
    }
    else if((firstHit<8 && color==0) || (firstHit>8 && color==1)) {
        faul = 0;
        return false;
    }
    else if(firstHit == 8) {
        bool canHitBlackBall = true;
        for(int i=1; i<16; i++) {
            if(color==0 && i>8) continue;
            if(color==1 && i<8) continue;
            if(game->balls[i]->isOnTable()) canHitBlackBall = false;
        }
        if(canHitBlackBall) {
            faul = 0;
            return false;
        }
    }

    faul = 1;
    game->balls[0]->getPosition() = Vector3(16.0f, 0.0f, 0.6f);
    game->balls[0]->isOnTable() = true;
    return true;
}

/**
 * This function is used to set white ball position after a faul.
 */
void PoolGame::setWhitePosition(Vector3 &pos) {
    Ball *ball = game->balls[0];
    Real r = ball->getR();

    if(pos[0] > sizeX/2.0 - r)
        pos[0] = sizeX/2.0 - r;
    else if(pos[0] < -sizeX/2.0 + r)
        pos[0] = -sizeX/2.0 + r;
    else if(pos[1] > sizeY/2.0 - r)
        pos[1] = sizeY/2.0 - r;
    else if(pos[1] < -sizeY/2.0 + r)
        pos[1] = -sizeY/2.0 + r;

    ball->getPosition() = pos;
}

/**
 * This method returns the number of player who is currently playing.
 */
int PoolGame::currentPlayer(void) {
    return player;
}

/**
 * This method returns what color should be hit be current player. It return 0 and 1 for colors and 2 if it is not yet defined.
 */
int PoolGame::currentColor(void) {
    return color;
}

/**
 * This method checks if the game has ended. It returns -1 if the game is still lasts or the winner number otherwise.
 */
int PoolGame::gameEnded(void) {
    if(!stopped) return -1;
    if(game->balls[8]->isOnTable()) return -1;
    if(isWhiteBallMovable()) return !player;

    bool canHitBlackBall = true;
    for(int i=1; i<16; i++) {
        if(color==0 && i>8) continue;
        if(color==1 && i<8) continue;
        if(game->balls[i]->isOnTable()) canHitBlackBall = false;
    }

    if(canHitBlackBall) return player;
    return !player;
}

/**
 * This function returns the position vector of selected ball.
 */
Vector3 PoolGame::getBallPosition(int ball) {
    return game->balls[ball]->getPosition();
}

/**
 * This functions returns quaternion that describes current ball orientation.
 */
Quaternion PoolGame::getBallOrientation(int ball) {
    return game->balls[ball]->getOrientation();
}

/**
 * This functions checks if a ball is in socket. Returns true if ball is on table.
 */
bool PoolGame::isBallOnTable(int ball) {
    return game->balls[ball]->isOnTable();
}

/**
 * This function is used to make a shot. It takes the rotations status, ball rotations and force as arguments.
 */
void PoolGame::shot(Real horDirection, Real vertDirection, Real horPoint, Real vertPoint, Real force) {
    if(isWhiteBallMovable()) {
        Table::BallVector::iterator iter;
        Ball *white = game->balls[0], *ball;

        for(iter = game->balls.begin(); iter!= game->balls.end(); iter++) {
            ball = *iter;

            if(ball->getID() == 0)
                continue;

            if((ball->getPosition() - white->getPosition()).length() < ball->getR() + white->getR())
                return;
        }
    }

    stopped = false;
    faul = 2;
    game->balls[0]->getFirstHit() = 0;
    game->balls[0]->shot(horDirection, force, Vector3(horPoint, vertPoint, vertDirection));
}

