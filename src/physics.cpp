/*
 * Copyright 2008 Jacek Caban
 * Copyright 2008 Piotr Caban
 * Copyright 2008 Jaroslaw Sobiecki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (aU your option) any later version.

 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <sys/time.h>

#include <Ogre.h>

#include "physics.h"

using namespace Ogre;

enum {
    BOUND_LEFT,
    BOUND_RIGHT,
    BOUND_BOTTOM,
    BOUND_UPPER
};

//#define DEBUG_PHYSICS

#ifdef DEBUG_PHYSICS

#define trace printf

#else

#define trace(...)

#endif

#define trace_vector(v) trace(#v ": %lf %lf %lf\n", (v)[0], (v)[1], (v)[2])
#define trace_quaternion(v) trace(#v ": %lf %lf %lf %lf\n", (v).w, (v).x, (v).y, (v).z)

const TableParameters TableParameters::DefaultTableParameters;

#define MAX_DT 0.01
#define ALMOST_ZERO 0.0001

static Ogre::Real inline deg2rad(Ogre::Real alpha) {
    return (alpha/360.0)*2.0*M_PI;
}

/**
 * Constructor of the Table class. Sets table and physics parameters.
 */
Table::Table(Real sx, Real sy, const TableParameters *p) : size_x(sx/2.0), size_y(sy/2.0), current_time(0), params(p)
{
    gettimeofday(&start_time, NULL);
}

/**
 * Updates state od balls diring calculated dt (small delta time).
 */
Real Table::eulerUpdate() {
    BallVector::iterator iter, iter2;
    Real dt = MAX_DT, tmp;
    Ball *ball = NULL, *ball2;
    int bound, bound_copy;

    enum {
        BALL_NONE,
        BALL_BOUND,
        BALL_BALL
    } action_type = BALL_NONE;

    trace("eulerUpdate\n");

    /* Find the next action */
    for(iter = balls.begin(); iter != balls.end(); iter++) {
        tmp = boundCrossTime(**iter, bound);
        if(tmp < dt) {
            action_type = BALL_BOUND;
            ball = *iter;
            bound_copy = bound;
            dt = tmp;
        }

        for(iter2 = iter+1; iter2 != balls.end(); iter2++) {
            tmp = ballCrossTime(**iter, **iter2);
            if(tmp < dt) {
                action_type = BALL_BALL;
                ball = *iter;
                ball2 = *iter2;
                dt = tmp;
            }
        }
    }

    trace("dt=%lf (%d)\n", dt, action_type);

    simpleUpdate(dt);

    switch(action_type) {
    case BALL_NONE:
        break;
    case BALL_BOUND:
        boundCross(*ball, bound_copy);
        break;
    case BALL_BALL:
        ballCross(*ball, *ball2);
        break;
    }

    return dt;
}

/**
 * Updates balls state depending on their states.
 */
void Table::simpleUpdate(Real dt) {
    BallVector::iterator iter;
    Vector3 norm, r = Vector3::ZERO, tmp, rotv, rotp, slow;
    Ball *ball;

    /* Slow down all balls (resistance from the table) */
    for(iter = balls.begin(); iter != balls.end(); iter++) {
        ball = *iter;
        norm = ball->speed.normalisedCopy();
        r[2] = -ball->r;

        trace("preUpdate:\n");
        trace_vector(ball->position);
        trace_vector(ball->speed);
        trace_vector(ball->rotation);
        trace_quaternion(ball->orientation);

        //Set ball orientation
        ball->orientation = Quaternion(Radian(ball->rotation.length()*dt), ball->rotation.normalisedCopy())
            * ball->orientation;
        
        ball->position += ball->speed*dt;
        tmp = params->slidingFriction * params->gravity * dt * norm;
        if(ball->speed.squaredLength() > tmp.squaredLength())
            ball->speed -= tmp;
        else
            ball->speed = Vector3::ZERO;

        tmp[0] = norm[1];
        tmp[1] = -norm[0];
        tmp[2] = 0.0;
        rotv = ball->rotation.dotProduct(tmp)*tmp;
        rotp = ball->rotation - rotv;
        tmp = r.crossProduct(rotv);
        slow = 10*Vector3::UNIT_Z.crossProduct(norm)*params->staticFriction*dt;
        trace_vector(rotv);
        trace_vector(rotp);
        trace_vector(tmp);
        trace_vector(slow);
        if(tmp.dotProduct(rotv) < 0.0 || tmp.squaredLength() < ball->speed.squaredLength()) {
            trace("rotup\n");
            rotv += slow;
        }else {
            trace("rotdown\n");
            rotv -= slow;
        }

        rotp -= rotp.normalisedCopy()*params->staticFriction*dt;
        ball->rotation = rotv+rotp;

        tmp = ball->rotation;
        tmp[2] = 0.0;
        tmp.normalise();
        ball->speed += -dt*0.01*params->staticFriction*tmp.crossProduct(r);

        if(ball->speed.squaredLength()<ball->rotation.squaredLength()) {
            Real ratio = ball->speed.length()/ball->rotation.length();
            ball->rotation *= ratio;
        }

        trace("simpleUpdate:\n");
        trace_vector(ball->position);
        trace_vector(ball->speed);
        trace_vector(ball->rotation);
    }
}

/**
 * Sets the ball to be fallen.
 */
inline void Table::ballFallIn(Ball &ball) {
    ball.position.z = -10.666;
    ball.onTable = false;
    ball.speed = ball.rotation = Vector3::ZERO;
}

/**
 * Handles crossing ball and bound event.
 */
void Table::boundCross(Ball &ball, int bound) {
    Real rot = ball.rotation.z*params->boundFriction*ball.speed.length();

    if((ball.position-Vector3(size_x, size_y, 0.6)).squaredLength() < params->pocketSize) {
        ballFallIn(ball);
        return;
    }
    else if((ball.position-Vector3(size_x, -size_y, 0.6)).squaredLength() < params->pocketSize) {
        ballFallIn(ball);
        return;
    }
    else if((ball.position-Vector3(0.0, size_y, 0.6)).squaredLength() < params->pocketSize) {
        ballFallIn(ball);
        return;
    }
    else if((ball.position-Vector3(0.0, -size_y, 0.6)).squaredLength() < params->pocketSize) {
        ballFallIn(ball);
        return;
    }
    else if((ball.position-Vector3(-size_x, size_y, 0.6)).squaredLength() < params->pocketSize) {
        ballFallIn(ball);
        return;
    }
    else if((ball.position-Vector3(-size_x, -size_y, 0.6)).squaredLength() < params->pocketSize) {
        ballFallIn(ball);
        return;
    }

    trace("boundCross\n");

    switch(bound) {
    case BOUND_LEFT:
        ball.speed[0] = -ball.speed[0];
        ball.speed[1] += rot;
        break;
    case BOUND_RIGHT:
        ball.speed[0] = -ball.speed[0];
        ball.speed[1] -= rot;
        break;
    case BOUND_BOTTOM:
        ball.speed[0] -= rot;
        ball.speed[1] = -ball.speed[1];
        break;
    case BOUND_UPPER:
        ball.speed[0] += rot;
        ball.speed[1] = -ball.speed[1];
        break;
    }

    ball.speed *= params->boundResistance;
    ball.rotation[2] = ALMOST_ZERO;
    ball.rotation *= 0.3;
}

/**
 * Handles two balls crossing event.
 */
void Table::ballCross(Ball &ball1, Ball &ball2) {
    Vector3 r, norm, v1x, v2x, v1y, v2y;

    if(!ball1.firstHit) ball1.firstHit = ball2.ballNo;
    if(!ball2.firstHit) ball2.firstHit = ball1.ballNo;

    trace("preBallCross:\n");
    trace_vector(ball1.position);
    trace_vector(ball1.speed);
    trace_vector(ball1.rotation);
    trace_vector(ball2.position);
    trace_vector(ball2.speed);
    trace_vector(ball2.rotation);

    r = ball1.position-ball2.position;
    norm[0] = r[1];
    norm[1] = -r[0];
    norm[2] = 0.0;
    norm.normalise();
    r *= 0.5;

    v1x = ball1.speed.dotProduct(norm) * norm;
    v2x = ball2.speed.dotProduct(norm) * norm;
    v1y = ball1.speed - v1x;
    v2y = ball2.speed - v2x;

    trace_vector(v1x);
    trace_vector(v2x);
    trace_vector(v1y);
    trace_vector(v2y);

    ball1.speed = v1x+v2y;
    ball2.speed = v2x+v1y;

    trace("ballCross:\n");
    trace_vector(ball1.position);
    trace_vector(ball1.speed);
    trace_vector(ball1.rotation);
    trace_vector(ball2.position);
    trace_vector(ball2.speed);
    trace_vector(ball2.rotation);
}

/**
 * Calculates time to the nearest ball and bound cross.
 */
Real Table::boundCrossTime(Ball &ball, int &bound) {
    Real ret, tmp;

    trace("boundCrossTime:\n");
    if(ball.speed[0] < -ALMOST_ZERO) {
        bound = BOUND_LEFT;
        ret = (-(size_x-ball.r) - ball.position[0]) / ball.speed[0];
        trace("left %lf\n", ret);
    }else if(ball.speed[0] > ALMOST_ZERO) {
        bound = BOUND_RIGHT;
        ret = (size_x-ball.r - ball.position[0]) / ball.speed[0];
        trace("right %lf\n", ret);
    }else {
        trace("left right no\n");
        ret = 100.0;
    }

    if(ball.speed[1] < -ALMOST_ZERO) {
        tmp = (-(size_y-ball.r) - ball.position[1]) / ball.speed[1];
        trace("bottom %lf\n", tmp);
        if(tmp < ret) {
            bound = BOUND_BOTTOM;
            ret = tmp;
        }
    }else if(ball.speed[1] > ALMOST_ZERO) {
        tmp = (size_y-ball.r - ball.position[1]) / ball.speed[1];
        trace("upper %lf\n", tmp);
        if(tmp < ret) {
            bound = BOUND_UPPER;
            ret = tmp;
        }
    }

    if(ret == 100.0)
        return ret;

    trace_vector(ball.position);
    trace_vector(ball.speed);
    trace_vector(ball.rotation);
    trace("ret %lf\n", ret);

    if(ret < 0.0)
        ret = 0.0;

    return ret;
}

/**
 * Calculates time to the nearest two balls cross.
 */
Real Table::ballCrossTime(Ball &ball1, Ball &ball2) {
    Vector3 dp, dp_next;
    Ball ball1_next = ball1;
    Ball ball2_next = ball2;

    ball1_next.position.x += ball1.speed.x*ALMOST_ZERO;
    ball1_next.position.y += ball1.speed.y*ALMOST_ZERO;
    ball2_next.position.x += ball2.speed.x*ALMOST_ZERO;
    ball2_next.position.y += ball2.speed.y*ALMOST_ZERO;

    dp = ball2.position - ball1.position;
    dp_next = ball2_next.position - ball1_next.position;

    if(dp.squaredLength()<dp_next.squaredLength()+ALMOST_ZERO*ALMOST_ZERO) return 100.0;
    if(dp.squaredLength()>4.0*ball1.r) return 100.0;

    return ALMOST_ZERO;
}

/**
 * Checks if there is any ball moving.
 */
bool Table::ballsMoving() {
    BallVector::iterator iter;
    Ball *ball;
    bool ret = false;

    for(iter = balls.begin(); iter != balls.end(); iter++) {
        ball = *iter;

        if(ball->speed.squaredLength() > ALMOST_ZERO) ret = true;
        else ball->speed = ball->rotation = Vector3::ZERO;
    }

    return ret;
}

static unsigned timeval2ms(const struct timeval &t) {
    return t.tv_sec*1000 + t.tv_usec/1000;
}

/**
 * Updates balls state to the given time.
 */
bool Table::update(Real time) {
    trace("update >\n");

    current_time += time;

    while(current_time>0.0) {
        if(!ballsMoving()) {
            trace("update <<\n");
            return false;
        }

        current_time -= eulerUpdate()/2.0;
    }

    return true;
}

/**
 * Handles shotting ball by cue.
 */
void Ball::shot(Real direction, Real force, Vector3 shot_pos, Real forceScale, Real hitTime) {
    Vector3 vr, dir;

    direction = deg2rad(direction);
    shot_pos[0] = deg2rad(shot_pos[0]);
    shot_pos[1] = deg2rad(shot_pos[1]);
    shot_pos[2] = deg2rad(shot_pos[2]);

    dir[0] = -Math::Cos(direction);
    dir[1] = -Math::Sin(direction);
    dir[2] = 0;

    trace("shot:   %lf %lf\n", direction, force);
    trace_vector(shot_pos);

    force *= forceScale;

    vr[0] = Math::Cos(direction + shot_pos[0]);
    vr[1] = Math::Sin(direction + shot_pos[0]);
    vr[2] = Math::Sin(shot_pos[1]);
    vr.normalise();
    vr *= r;

    /* v' = (-k dt)/m * x */
    speed = hitTime/mass*dir*force;

    dir *= -r;

    /* w' = 2.5 * dt / (m r2) (r x (-kx)) */
    rotation = 2.5 * (20.0 * hitTime / (mass * r*r)) * (vr.crossProduct(dir));

    trace_vector(position);
    trace_vector(speed);
    trace_vector(rotation);
}
