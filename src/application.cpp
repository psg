/*
 * Copyright 2008 Jacek Caban
 * Copyright 2008 Piotr Caban
 * Copyright 2008 Jarek Sobiecki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "rules.h"
#include "ui.h"

#include <Ogre.h>
#include <OIS/OIS.h>
#include <CEGUI/CEGUI.h>
#include <OgreCEGUIRenderer.h>

using namespace Ogre;

/**
 * This is the main program class. It is created when the program is run and destroyed when the program quits.
 */
class Application {
public:
    void go();
    ~Application();

private:
    Ogre::Root *mRoot;
    OIS::Keyboard *mKeyboard;
    OIS::Mouse *mMouse;
    OIS::InputManager *mInputManager;
    CEGUI::OgreCEGUIRenderer *mRenderer;
    CEGUI::System *mSystem;
    UI *mUI;
    bool mContinue;
    CEGUI::WindowManager *mWindowManager;
    Ogre::FrameListener *mListener;
    MenuFrameListener *mMenuListener;
    PoolGame *game;
    
    void createRoot();
    void defineResources();
    void setupRenderSystem();
    void createRenderWindow();
    void initializeResourceGroups();
    void setupScene();
    void setupInputSystem();
    void setupCEGUI();
    void createFrameListener();
    void startRenderLoop();
};

/**
 * This is the main game listener. It is responsible for interaction with user during the game (not in menu).
 * More informations about keys/how to play can be found in documentation.pdf.
 * This class interacts with PoolGame class (rules) and UI (for menu integration).
 */
class GameListener : public FrameListener, public OIS::MouseListener {
private:
    Real getMoveScale();

    static const Real ROTATION_SPEED = 10.0;
    static const Real ZOOM_SPEED = 0.2;
    static const Real MOVE_SPEED = 0.1;
    static const Real ZOOM_MIN = 0.1;
    static const Real ZOOM_MAX = 2.0;

public:
    /**
     * GameListener class constructor.
     */
    GameListener(OIS::Keyboard *keyboard, OIS::Mouse *mouse, SceneManager *mgr, PoolGame *sGame, UI *sUI, MouseListener *mouseListener) {
        mUI = sUI;
        mKeyboard = keyboard;
        mMouse = mouse;
        mToggle = 0.0f;
        mSceneManager = mgr;
        game = sGame;
        mCamera = mgr->getCamera("Camera");
        mCameraLocation = 0;
        vertical = 10.0f; horizontal = 0.0f;
        ballV = 0.0f; ballH = 0.0f;
        zoom = 1.0f;
        mMenuListener = mouseListener;
        mMousePressed = false;
        mLastWhellPosition = 0;
        mLastXPosition = 0;
        mLastYPosition = 0;
    }

    /**
     * Implementation of MouseListener::MouseMoved event.
     * It translates OIS event to CEGUI event. If right mouse button is pressed the function
     * rotates whole scene. It's responsible for handling mouse whell move too.
     */
    bool mouseMoved(const OIS::MouseEvent &arg)
    {
      Real deg = 0.0;
      int sgn = 1;
      int delta_zoom = arg.state.Z.abs - mLastWhellPosition;
      int delta_x = arg.state.X.abs - mLastXPosition;
      int delta_y = arg.state.Y.abs - mLastYPosition;
      //first, we have to handle mouse whell move 
      if (delta_zoom < 0 && (zoom + (delta_zoom)*mTimeSinceLastFrame*0.2) > ZOOM_MIN ||
          delta_zoom > 0 && (zoom + (delta_zoom)*mTimeSinceLastFrame*0.2) < ZOOM_MAX)
      {
        zoom += (delta_zoom)*mTimeSinceLastFrame*0.2;
      }
      mSceneManager->getSceneNode("CameraWhiteNode")->setPosition(50.0f*zoom, 0.0f, 0.0f);
      mSceneManager->getSceneNode("CameraTableNode")->setPosition(70.0f*zoom, 0.0f, 0.0f);
      mSceneManager->getSceneNode("CrossNode")->setScale(Vector3(0.05f*zoom, 0.05f*zoom, 0.05f*zoom));
      mLastWhellPosition = arg.state.Z.abs;

     if (!mMousePressed)
     {
       CEGUI::System::getSingleton().injectMousePosition(arg.state.X.abs*2, arg.state.Y.abs*2);
     }
     else
     {

       //TODO: ARCBALL
       if((delta_y < 0 && vertical > 10.0f) ||
          (delta_y > 0 && vertical < 80.0f)) {
            sgn = delta_y > 0 ? -1.0 : +1.0;
            deg = 360.0*fabs(delta_y)*mTimeSinceLastFrame;
            mSceneManager->getSceneNode("CameraLookAtTableNode")->rotate(Vector3::UNIT_Y, sgn * Degree(deg));
            mSceneManager->getSceneNode("CameraLookAtWhiteNode")->rotate(Vector3::UNIT_Y, sgn * Degree(deg));
            mSceneManager->getSceneNode("CrossVerticalRotationNode")->rotate(Vector3::UNIT_Y, sgn * Degree(deg));
            vertical += -1.0 * sgn * deg;
        }
      
      sgn = delta_x > 0 ? -1.0 : +1.0;

      deg = 360.0*fabs(delta_x)*mTimeSinceLastFrame;
      if(mCameraLocation) mSceneManager->getSceneNode("CameraTableRotationNode")->rotate(Vector3::UNIT_Z, sgn *Degree(deg));
      else {
          mSceneManager->getSceneNode("CameraWhiteRotationNode")->rotate(Vector3::UNIT_Z, sgn * Degree(deg));
          horizontal += sgn * deg;
      }

     }

     mLastYPosition = arg.state.Y.abs;
     mLastXPosition = arg.state.X.abs;

     return true;

    }

    /**
     * This method is implementation of MouseListener::MousePressed  method.
     * It detects right mouse button press and change mouse cursor.
     * It also translate OIS click event to CEGUI event
     */
    bool mousePressed(const OIS::MouseEvent &arg, OIS::MouseButtonID id)
    {
      switch (id)
      {
        case OIS::MB_Button3:
          break;
        case OIS::MB_Button4:
          break;
        case OIS::MB_Button5:
          break;
        case OIS::MB_Button6:
          break;
        case OIS::MB_Button7:
          break;
        case OIS::MB_Right:
          CEGUI::System::getSingletonPtr()->setDefaultMouseCursor((CEGUI::utf8*) "PSG",
                                                                  (CEGUI::utf8*) "MouseMoveCursor");

          mMousePressed = true;
          
          break;
        case OIS::MB_Middle:
            break;
      }
      CEGUI::System::getSingleton().injectMouseButtonDown(
          MenuFrameListener::convertButton(id));
      return true;
    }


    /**
     *  It's implementation of MouseListener::MouseReleased method
     *  It't responsible for changing view of cursor
     */
    bool mouseReleased(const OIS::MouseEvent &arg, OIS::MouseButtonID id)
    {
      switch (id)
      {
        case OIS::MB_Button3:
          break;
        case OIS::MB_Button4:
          break;
        case OIS::MB_Button5:
          break;
        case OIS::MB_Button6:
          break;
        case OIS::MB_Button7:
          break;
        case OIS::MB_Right:
          mMousePressed = false;
          CEGUI::System::getSingletonPtr()->setDefaultMouseCursor((CEGUI::utf8*) "PSG",
                                                                  (CEGUI::utf8*) "MouseArrow");
          break;
        case OIS::MB_Middle:
            break;
      }
      CEGUI::System::getSingleton().injectMouseButtonUp(
          MenuFrameListener::convertButton(id));
      return true;

      return true;
      return false;
    }

    /**
     * This function is started every time before the frame is rendered. It checks if the user is pressing any key.
     * It is responsible for ball placing as well.
     *
     * frameStarted function do following things:
     *  - rotates camera
     *  - zomms camera in/out
     *  - sets current camera
     *  - places withe ball after a faul
     *  - changes camera
     *  - switches on/off lights and shadows
     *  - sets balls orientation and position
     */
    bool frameStarted(const FrameEvent &evt) {
        Real deg;
        mTimeSinceLastFrame = evt.timeSinceLastFrame;

        if(mUI->getScene() != M_DISABLED) 
        {
          mMouse->setEventCallback(mMenuListener);
          return true;
        }
        mMouse->setEventCallback(this);
        mKeyboard->capture();
        mMouse->capture();

        mToggle -= evt.timeSinceLastFrame;

        if(mToggle<0.0f && mKeyboard->isKeyDown(OIS::KC_C)) {
            mToggle = 0.5f;
            mCamera->getParentSceneNode()->detachObject(mCamera);
            mCameraLocation = !mCameraLocation;
            if(mCameraLocation) mSceneManager->getSceneNode("CameraTableNode")->attachObject(mCamera);
            else mSceneManager->getSceneNode("CameraWhiteNode")->attachObject(mCamera);
        }
        else if(mToggle<0.0f && mKeyboard->isKeyDown(OIS::KC_1)) {
            mToggle = 0.5f;
            mCamera->getParentSceneNode()->detachObject(mCamera);
            mSceneManager->getSceneNode("CameraWhiteNode")->attachObject(mCamera);
            mCameraLocation = 0;
        }
        else if(mToggle<0.0f && mKeyboard->isKeyDown(OIS::KC_2)) {
            mToggle = 0.5f;
            mCamera->getParentSceneNode()->detachObject(mCamera);
            mSceneManager->getSceneNode("CameraTableNode")->attachObject(mCamera);
            mCameraLocation = 1;
        }
        else if(!game->isBallOnTable(0) || (mToggle<0.0f && mKeyboard->isKeyDown(OIS::KC_3))) {
            mToggle = 0.5f;
            mCamera->getParentSceneNode()->detachObject(mCamera);
            mSceneManager->getSceneNode("CameraTableNode")->attachObject(mCamera);
            mCameraLocation = 1;
            mSceneManager->getSceneNode("CameraLookAtTableNode")->rotate(Vector3::UNIT_Y, Degree(vertical-80.0));
            mSceneManager->getSceneNode("CameraLookAtWhiteNode")->rotate(Vector3::UNIT_Y, Degree(vertical-80.0));
            mSceneManager->getSceneNode("CrossVerticalRotationNode")->rotate(Vector3::UNIT_Y, -Degree(vertical-80.0));
            vertical += 80.0-vertical;
            zoom = (ZOOM_MAX+ZOOM_MIN)/2.0;
            mSceneManager->getSceneNode("CameraWhiteNode")->setPosition(50.0f*zoom, 0.0f, 0.0f);
            mSceneManager->getSceneNode("CameraTableNode")->setPosition(70.0f*zoom, 0.0f, 0.0f);
            mSceneManager->getSceneNode("CrossNode")->setScale(Vector3(0.05f*zoom, 0.05f*zoom, 0.05f*zoom));
        }
        else if(mToggle<0.0f && mKeyboard->isKeyDown(OIS::KC_K)) {
            mToggle = 0.5f;
            mSceneManager->getLight("Light1")->setCastShadows(!mSceneManager->getLight("Light1")->getCastShadows());
            mSceneManager->getLight("Light2")->setCastShadows(!mSceneManager->getLight("Light2")->getCastShadows());
            mSceneManager->getLight("Light3")->setCastShadows(!mSceneManager->getLight("Light3")->getCastShadows());
        }
        else if(mToggle<0.0f && mKeyboard->isKeyDown(OIS::KC_L)) {
            mToggle = 0.5f;
            mSceneManager->getLight("Light1")->setVisible(!mSceneManager->getLight("Light1")->getVisible());
            mSceneManager->getLight("Light2")->setVisible(!mSceneManager->getLight("Light2")->getVisible());
            mSceneManager->getLight("Light3")->setVisible(!mSceneManager->getLight("Light3")->getVisible());
        }

        if(mKeyboard->isKeyDown(OIS::KC_DOWN) && vertical<80.0f) {
            deg = ROTATION_SPEED*evt.timeSinceLastFrame*getMoveScale();
            mSceneManager->getSceneNode("CameraLookAtTableNode")->rotate(Vector3::UNIT_Y, -Degree(deg));
            mSceneManager->getSceneNode("CameraLookAtWhiteNode")->rotate(Vector3::UNIT_Y, -Degree(deg));
            mSceneManager->getSceneNode("CrossVerticalRotationNode")->rotate(Vector3::UNIT_Y, -Degree(deg));
            vertical += deg;
        }
        else if(mKeyboard->isKeyDown(OIS::KC_UP) && vertical>10.0f) {
            deg = ROTATION_SPEED*evt.timeSinceLastFrame*getMoveScale();;
            mSceneManager->getSceneNode("CameraLookAtTableNode")->rotate(Vector3::UNIT_Y, Degree(deg));
            mSceneManager->getSceneNode("CameraLookAtWhiteNode")->rotate(Vector3::UNIT_Y, Degree(deg));
            mSceneManager->getSceneNode("CrossVerticalRotationNode")->rotate(Vector3::UNIT_Y, Degree(deg));
            vertical -= deg;
        }
        else if(mKeyboard->isKeyDown(OIS::KC_RIGHT)) {
            deg = ROTATION_SPEED*evt.timeSinceLastFrame*getMoveScale();
            if(mCameraLocation) mSceneManager->getSceneNode("CameraTableRotationNode")->rotate(Vector3::UNIT_Z, -Degree(deg));
            else {
                mSceneManager->getSceneNode("CameraWhiteRotationNode")->rotate(Vector3::UNIT_Z, -Degree(deg));
                horizontal -= deg;
            }
        }
        else if(mKeyboard->isKeyDown(OIS::KC_LEFT)) {
            deg = ROTATION_SPEED*evt.timeSinceLastFrame*getMoveScale();
            if(mCameraLocation) mSceneManager->getSceneNode("CameraTableRotationNode")->rotate(Vector3::UNIT_Z, Degree(deg));
            else {
                mSceneManager->getSceneNode("CameraWhiteRotationNode")->rotate(Vector3::UNIT_Z, Degree(deg));
                horizontal += deg;
            }
        }
        else if(mKeyboard->isKeyDown(OIS::KC_W) && !mCameraLocation && ballV<40.0f) {
            deg = ROTATION_SPEED*evt.timeSinceLastFrame*getMoveScale();
            mSceneManager->getSceneNode("CrossVerticalRotationNode")->rotate(Vector3::UNIT_Y, -Degree(deg));
            ballV += deg;
        }
        else if(mKeyboard->isKeyDown(OIS::KC_S) && !mCameraLocation && ballV>-40.0f) {
            deg = ROTATION_SPEED*evt.timeSinceLastFrame*getMoveScale();
            mSceneManager->getSceneNode("CrossVerticalRotationNode")->rotate(Vector3::UNIT_Y, Degree(deg));
            ballV -= deg;
        }
        else if(mKeyboard->isKeyDown(OIS::KC_A) && !mCameraLocation && ballH<40.0f) {
            deg = ROTATION_SPEED*evt.timeSinceLastFrame*getMoveScale();
            mSceneManager->getSceneNode("CrossRotationNode")->rotate(Vector3::UNIT_Z, -Degree(deg));
            ballH += deg;
        }
        else if(mKeyboard->isKeyDown(OIS::KC_D) && !mCameraLocation && ballH>-40.0f) {
            deg = ROTATION_SPEED*evt.timeSinceLastFrame*getMoveScale();
            mSceneManager->getSceneNode("CrossRotationNode")->rotate(Vector3::UNIT_Z, Degree(deg));
            ballH -= deg;
        }

        if(mKeyboard->isKeyDown(OIS::KC_EQUALS) && zoom>ZOOM_MIN) {
            zoom -= ZOOM_SPEED*evt.timeSinceLastFrame;
            mSceneManager->getSceneNode("CameraWhiteNode")->setPosition(50.0f*zoom, 0.0f, 0.0f);
            mSceneManager->getSceneNode("CameraTableNode")->setPosition(70.0f*zoom, 0.0f, 0.0f);
            mSceneManager->getSceneNode("CrossNode")->setScale(Vector3(0.05f*zoom, 0.05f*zoom, 0.05f*zoom));
        }
        else if(mKeyboard->isKeyDown(OIS::KC_MINUS) && zoom<ZOOM_MAX) {
            zoom += ZOOM_SPEED*evt.timeSinceLastFrame;
            mSceneManager->getSceneNode("CameraWhiteNode")->setPosition(50.0f*zoom, 0.0f, 0.0f);
            mSceneManager->getSceneNode("CameraTableNode")->setPosition(70.0f*zoom, 0.0f, 0.0f);
            mSceneManager->getSceneNode("CrossNode")->setScale(Vector3(0.05f*zoom, 0.05f*zoom, 0.05f*zoom));
        }
        else if (mKeyboard->isKeyDown(OIS::KC_ESCAPE)) {
            mUI->setScene(M_IN_GAME_MENU);
        }

        if(game->isWaitingForShot(evt.timeSinceLastFrame)) {
            if(game->isWhiteBallMovable()) {
                Vector3 pos = game->getBallPosition(0);

                mSceneManager->getSceneNode("WhiteNode")->setVisible(game->isBallOnTable(0));

                if(mKeyboard->isKeyDown(OIS::KC_F)) {
                    pos[1] -= MOVE_SPEED*getMoveScale();
                    game->setWhitePosition(pos);
                }else if(mKeyboard->isKeyDown(OIS::KC_H)) {
                    pos[1] += MOVE_SPEED*getMoveScale();
                    game->setWhitePosition(pos);
                }else if(mKeyboard->isKeyDown(OIS::KC_T)) {
                    pos[0] -= MOVE_SPEED*getMoveScale();
                    game->setWhitePosition(pos);
                }else if(mKeyboard->isKeyDown(OIS::KC_G)) {
                    pos[0] += MOVE_SPEED*getMoveScale();
                    game->setWhitePosition(pos);
                }

                mSceneManager->getSceneNode("WhiteNode")->setPosition(pos);
            }
            if(mCameraLocation) mSceneManager->getEntity("Cross")->setVisible(false);
            else mSceneManager->getEntity("Cross")->setVisible(true);

            if (mKeyboard->isKeyDown(OIS::KC_SPACE) && !mCameraLocation)
            {
                game->shot(horizontal, vertical, ballH, ballV, mUI->getPowerShot());
            }
        
        }
        else {
            mSceneManager->getSceneNode("CrossRotationNode")->rotate(Vector3::UNIT_Z, Degree(ballH));
            mSceneManager->getSceneNode("CrossVerticalRotationNode")->rotate(Vector3::UNIT_Y, Degree(ballV));
            ballH = 0.0f;
            ballV = 0.0f;
            mSceneManager->getSceneNode("WhiteNode")->setVisible(game->isBallOnTable(0));
            mSceneManager->getSceneNode("WhiteNode")->setPosition(game->getBallPosition(0));
            mSceneManager->getSceneNode("WhiteRotationNode")->setOrientation(game->getBallOrientation(0));
            mSceneManager->getSceneNode("Ball1Node")->setVisible(game->isBallOnTable(1));
            mSceneManager->getSceneNode("Ball1Node")->setPosition(game->getBallPosition(1));
            mSceneManager->getSceneNode("Ball1RotationNode")->setOrientation(game->getBallOrientation(1));
            mSceneManager->getSceneNode("Ball2Node")->setVisible(game->isBallOnTable(2));
            mSceneManager->getSceneNode("Ball2Node")->setPosition(game->getBallPosition(2));
            mSceneManager->getSceneNode("Ball2RotationNode")->setOrientation(game->getBallOrientation(2));
            mSceneManager->getSceneNode("Ball3Node")->setVisible(game->isBallOnTable(3));
            mSceneManager->getSceneNode("Ball3Node")->setPosition(game->getBallPosition(3));
            mSceneManager->getSceneNode("Ball3RotationNode")->setOrientation(game->getBallOrientation(3));
            mSceneManager->getSceneNode("Ball4Node")->setVisible(game->isBallOnTable(4));
            mSceneManager->getSceneNode("Ball4Node")->setPosition(game->getBallPosition(4));
            mSceneManager->getSceneNode("Ball4RotationNode")->setOrientation(game->getBallOrientation(4));
            mSceneManager->getSceneNode("Ball5Node")->setVisible(game->isBallOnTable(5));
            mSceneManager->getSceneNode("Ball5Node")->setPosition(game->getBallPosition(5));
            mSceneManager->getSceneNode("Ball5RotationNode")->setOrientation(game->getBallOrientation(5));
            mSceneManager->getSceneNode("Ball6Node")->setVisible(game->isBallOnTable(6));
            mSceneManager->getSceneNode("Ball6Node")->setPosition(game->getBallPosition(6));
            mSceneManager->getSceneNode("Ball6RotationNode")->setOrientation(game->getBallOrientation(6));
            mSceneManager->getSceneNode("Ball7Node")->setVisible(game->isBallOnTable(7));
            mSceneManager->getSceneNode("Ball7Node")->setPosition(game->getBallPosition(7));
            mSceneManager->getSceneNode("Ball7RotationNode")->setOrientation(game->getBallOrientation(7));
            mSceneManager->getSceneNode("Ball8Node")->setVisible(game->isBallOnTable(8));
            mSceneManager->getSceneNode("Ball8Node")->setPosition(game->getBallPosition(8));
            mSceneManager->getSceneNode("Ball8RotationNode")->setOrientation(game->getBallOrientation(8));
            mSceneManager->getSceneNode("Ball9Node")->setVisible(game->isBallOnTable(9));
            mSceneManager->getSceneNode("Ball9Node")->setPosition(game->getBallPosition(9));
            mSceneManager->getSceneNode("Ball9RotationNode")->setOrientation(game->getBallOrientation(9));
            mSceneManager->getSceneNode("Ball10Node")->setVisible(game->isBallOnTable(10));
            mSceneManager->getSceneNode("Ball10Node")->setPosition(game->getBallPosition(10));
            mSceneManager->getSceneNode("Ball10RotationNode")->setOrientation(game->getBallOrientation(10));
            mSceneManager->getSceneNode("Ball11Node")->setVisible(game->isBallOnTable(11));
            mSceneManager->getSceneNode("Ball11Node")->setPosition(game->getBallPosition(11));
            mSceneManager->getSceneNode("Ball11RotationNode")->setOrientation(game->getBallOrientation(11));
            mSceneManager->getSceneNode("Ball12Node")->setVisible(game->isBallOnTable(12));
            mSceneManager->getSceneNode("Ball12Node")->setPosition(game->getBallPosition(12));
            mSceneManager->getSceneNode("Ball12RotationNode")->setOrientation(game->getBallOrientation(12));
            mSceneManager->getSceneNode("Ball13Node")->setVisible(game->isBallOnTable(13));
            mSceneManager->getSceneNode("Ball13Node")->setPosition(game->getBallPosition(13));
            mSceneManager->getSceneNode("Ball13RotationNode")->setOrientation(game->getBallOrientation(13));
            mSceneManager->getSceneNode("Ball14Node")->setVisible(game->isBallOnTable(14));
            mSceneManager->getSceneNode("Ball14Node")->setPosition(game->getBallPosition(14));
            mSceneManager->getSceneNode("Ball14RotationNode")->setOrientation(game->getBallOrientation(14));
            mSceneManager->getSceneNode("Ball15Node")->setVisible(game->isBallOnTable(15));
            mSceneManager->getSceneNode("Ball15Node")->setPosition(game->getBallPosition(15));
            mSceneManager->getSceneNode("Ball15RotationNode")->setOrientation(game->getBallOrientation(15));
            mSceneManager->getEntity("Cross")->setVisible(false);
        }
        return true;
    }

private:

    //! OIS keyboard handle
    OIS::Keyboard *mKeyboard; 
    //! OIS mouse handle
    OIS::Mouse *mMouse;
    Real mToggle;

    //! It's pointer to UI class, responsible for drawing 2D interface.
    UI *mUI;
    //! Whell position in last frame, used for whell position change calculation
    int mLastWhellPosition;

    //! Vartical mouse position in last frame, used for mouse position change calculation
    int mLastYPosition;

    //! Horizontal mouse position in last frame, used for mouse position change calculation
    int mLastXPosition;

    //! OGRE camera handle
    Camera *mCamera;
    //! OGRE game scene manager
    SceneManager *mSceneManager;
    //! Class with game state
    PoolGame *game;
    //! Vertical angle the camera is facing
    int mCameraLocation;

    //! Decribes camera vertical position
    //! Camera horizontal position
    //! White ball rotations settings (horizontal)
    //! White ball rotations settings (vertical)
    Real vertical, horizontal, ballH, ballV;
    //! Currently set zoom
    Real zoom;
    //! This member is toggled by mousePressed and mouseReleased methods.
    bool mMousePressed;

    //! This member means amount of time from last rendered frame
    Real mTimeSinceLastFrame;

    //! This is pointer to second FrameListener class, which inherit also MouseListener class in order to handle some OIS events
    MouseListener *mMenuListener;
};

/**
 * This method returns how the actions taken by user should be scalled (how fast camera moves after pressing a button).
 */
Real GameListener::getMoveScale() {
    if(mKeyboard->isKeyDown(OIS::KC_LCONTROL))
        return 0.1;
    if(mKeyboard->isKeyDown(OIS::KC_LMENU))
        return 10.0;
    return 1.0;
}

/**
 * This function is run when the game starts. It initializes all the structures, data.
 */
void Application::go() {
    mInputManager = NULL;
    mSystem = NULL;
    mRenderer = NULL;
    mUI = NULL;
    mListener = NULL;
    mRoot = NULL;

    game = new PoolGame();

    createRoot();
    defineResources();
    setupRenderSystem();
    createRenderWindow();
    initializeResourceGroups();
    setupScene();
    setupInputSystem();
    setupCEGUI();
    createFrameListener();
    startRenderLoop();
}

/**
 * Main game desctructor. It is run when the game quits.
 */
Application::~Application() {
    if(mInputManager) {
        mInputManager->destroyInputObject(mKeyboard);
        mInputManager->destroyInputObject(mMouse);
        OIS::InputManager::destroyInputSystem(mInputManager);
    }
    if(mSystem) delete mSystem;
    if(mRenderer) delete mRenderer;
    if(mUI) delete mUI;
    if(mListener) delete mListener;
    if(mRoot) delete mRoot;
}

/**
 * This function is used to initialze OGRE engine. It creates scene root node.
 */
void Application::createRoot() {
    mRoot = new Root();
}

/**
 * This function loads the resources defined in resource.cfg.
 */
void Application::defineResources() {
    String secName, typeName, archName;
    ConfigFile cf;
    cf.load("resources.cfg");

    ConfigFile::SectionIterator seci = cf.getSectionIterator();
    while (seci.hasMoreElements()) {
        secName = seci.peekNextKey();
        ConfigFile::SettingsMultiMap *settings = seci.getNext();
        ConfigFile::SettingsMultiMap::iterator i;
        for (i = settings->begin(); i != settings->end(); ++i) {
            typeName = i->first;
            archName = i->second;
            ResourceGroupManager::getSingleton().addResourceLocation(archName, typeName, secName);
        }
    }
}

/**
 * This function is responsible for the dialog that is shown when the game starts. The user can set some graphics options in it.
 */
void Application::setupRenderSystem() {
    if(!mRoot->showConfigDialog())
        throw Exception(52, "User canceled the config dialog!", "Application::setupRenderSystem()");
}

/**
 * This function creates the game window.
 */
void Application::createRenderWindow() {
    mRoot->initialise(true, "Pool Simulation Game");
}

/**
 * This function sets some options connected to resources (e.g. mipmap points).
 * It loads all objects to memory as well.
 */
void Application::initializeResourceGroups() {
    TextureManager::getSingleton().setDefaultNumMipmaps(5);
    ResourceGroupManager::getSingleton().initialiseAllResourceGroups();
}

/**
 * This function places the objects in the scene. It sets the positions, creates scene nodes so we can easily rotate and move them.
 * This function defines lights as well.
 */
void Application::setupScene() {
    SceneManager *mgr = mRoot->createSceneManager(ST_GENERIC, "SceneManager");
    Camera *cam = mgr->createCamera("Camera");
    Viewport *vp = mRoot->getAutoCreatedWindow()->addViewport(cam);

    cam->lookAt(Vector3(-1.0f, 0.0f, 0.0f));
    cam->setNearClipDistance(0.01f);
    cam->roll(Degree(90));
    mgr->setAmbientLight(ColourValue(1.0f, 1.0f, 1.0f));

    mgr->setShadowTechnique(SHADOWTYPE_STENCIL_ADDITIVE);//SHADOWTYPE_TEXTURE_MODULATIVE);
    mgr->setShadowColour(ColourValue(0.5f,0.5f,0.5f));

    /* Rozmiar stolu 60x30 */
    Entity *table = mgr->createEntity("PoolTable", "table.mesh");
    table->setCastShadows(false);
    Entity *white = mgr->createEntity("White", "white.mesh");
    Entity *ball1 = mgr->createEntity("Ball1", "ball1.mesh");
    Entity *ball2 = mgr->createEntity("Ball2", "ball2.mesh");
    Entity *ball3 = mgr->createEntity("Ball3", "ball3.mesh");
    Entity *ball4 = mgr->createEntity("Ball4", "ball4.mesh");
    Entity *ball5 = mgr->createEntity("Ball5", "ball5.mesh");
    Entity *ball6 = mgr->createEntity("Ball6", "ball6.mesh");
    Entity *ball7 = mgr->createEntity("Ball7", "ball7.mesh");
    Entity *ball8 = mgr->createEntity("Ball8", "ball8.mesh");
    Entity *ball9 = mgr->createEntity("Ball9", "ball9.mesh");
    Entity *ball10 = mgr->createEntity("Ball10", "ball10.mesh");
    Entity *ball11 = mgr->createEntity("Ball11", "ball11.mesh");
    Entity *ball12 = mgr->createEntity("Ball12", "ball12.mesh");
    Entity *ball13 = mgr->createEntity("Ball13", "ball13.mesh");
    Entity *ball14 = mgr->createEntity("Ball14", "ball14.mesh");
    Entity *ball15 = mgr->createEntity("Ball15", "ball15.mesh");
    Entity *cross = mgr->createEntity("Cross", "cross.mesh");

    SceneNode *tableNode = mgr->getRootSceneNode()->createChildSceneNode("TableNode");
    SceneNode *tableCenterNode = tableNode->createChildSceneNode("TableCenterNode", Vector3(1.1f, -2.0f, -20.0f));
    SceneNode *whiteNode = mgr->getRootSceneNode()->createChildSceneNode("WhiteNode", Vector3(0.0f, 0.0f, 0.6f));
    SceneNode *whiteRotationNode = whiteNode->createChildSceneNode("WhiteRotationNode");
    SceneNode *whiteCenterNode = whiteRotationNode->createChildSceneNode("WhiteCenterNode", Vector3(-12.96f, -4.2f, -20.6f));
    SceneNode *cameraTableRotationNode = tableNode->createChildSceneNode("CameraTableRotationNode", Vector3(0.0f, 0.0f, 1.0f));
    SceneNode *cameraWhiteRotationNode = whiteNode->createChildSceneNode("CameraWhiteRotationNode", Vector3(0.0f, 0.0f, 1.0f));
    SceneNode *cameraLookAtTableNode = cameraTableRotationNode->createChildSceneNode("CameraLookAtTableNode");
    SceneNode *cameraLookAtWhiteNode = cameraWhiteRotationNode->createChildSceneNode("CameraLookAtWhiteNode");
    SceneNode *cameraWhiteNode = cameraLookAtWhiteNode->createChildSceneNode("CameraWhiteNode", Vector3(50.0f, 0.0f, 0.0f));
    SceneNode *cameraTableNode = cameraLookAtTableNode->createChildSceneNode("CameraTableNode", Vector3(70.0f, 0.0f, 0.0f));
    cameraLookAtTableNode->rotate(Vector3::UNIT_Y, -Degree(10));
    cameraLookAtWhiteNode->rotate(Vector3::UNIT_Y, -Degree(10));
    tableCenterNode->attachObject(table);
    whiteCenterNode->attachObject(white);
    cameraWhiteNode->attachObject(cam);
    whiteNode->setVisible(false);

    SceneNode *crossRotationNode = cameraWhiteRotationNode->createChildSceneNode("CrossRotationNode");
    SceneNode *crossVerticalRotationNode = crossRotationNode->createChildSceneNode("CrossVerticalRotationNode", Vector3(0.0f, 0.0f, -1.0f));
    SceneNode *crossNode = crossVerticalRotationNode->createChildSceneNode("CrossNode", Vector3(1.0f, 0.0f, 0.0f));
    crossRotationNode->rotate(Vector3::UNIT_Y, -Degree(10));
    crossNode->setScale(Vector3(0.05f, 0.05f, 0.05f));
    crossNode->attachObject(cross);

    SceneNode *ball1Node = mgr->getRootSceneNode()->createChildSceneNode("Ball1Node", Vector3(0.0f, 0.0f, 0.6f));
    SceneNode *ball1RotationNode = ball1Node->createChildSceneNode("Ball1RotationNode");
    SceneNode *ball1CenterNode = ball1RotationNode->createChildSceneNode("Ball1CenterNode", Vector3(18.7f, -2.15f, -20.6f));
    ball1CenterNode->attachObject(ball1);
    ball1Node->setVisible(false);

    SceneNode *ball2Node = mgr->getRootSceneNode()->createChildSceneNode("Ball2Node", Vector3(0.0f, 0.0f, 0.6f));
    SceneNode *ball2RotationNode = ball2Node->createChildSceneNode("Ball2RotationNode");
    SceneNode *ball2CenterNode = ball2RotationNode->createChildSceneNode("Ball2CenterNode", Vector3(25.82f, -0.33f, -20.6f));
    ball2CenterNode->attachObject(ball2);
    ball2Node->setVisible(false);

    SceneNode *ball3Node = mgr->getRootSceneNode()->createChildSceneNode("Ball3Node", Vector3(0.0f, 0.0f, 0.6f));
    SceneNode *ball3RotationNode = ball3Node->createChildSceneNode("Ball3RotationNode");
    SceneNode *ball3CenterNode = ball3RotationNode->createChildSceneNode("Ball3CenterNode", Vector3(20.40f, -1.17f, -20.6f));
    ball3CenterNode->attachObject(ball3);
    ball3Node->setVisible(false);

    SceneNode *ball4Node = mgr->getRootSceneNode()->createChildSceneNode("Ball4Node", Vector3(0.0f, 0.0f, 0.6f));
    SceneNode *ball4RotationNode = ball4Node->createChildSceneNode("Ball4RotationNode");
    SceneNode *ball4CenterNode = ball4RotationNode->createChildSceneNode("Ball4CenterNode", Vector3(22.08f, -4.30f, -20.6f));
    ball4CenterNode->attachObject(ball4);
    ball4Node->setVisible(false);

    SceneNode *ball5Node = mgr->getRootSceneNode()->createChildSceneNode("Ball5Node", Vector3(0.0f, 0.0f, 0.6f));
    SceneNode *ball5RotationNode = ball5Node->createChildSceneNode("Ball5RotationNode");
    SceneNode *ball5CenterNode = ball5RotationNode->createChildSceneNode("Ball5CenterNode", Vector3(23.83f, -5.37f, -20.6f));
    ball5CenterNode->attachObject(ball5);
    ball5Node->setVisible(false);

    SceneNode *ball6Node = mgr->getRootSceneNode()->createChildSceneNode("Ball6Node", Vector3(0.0f, 0.0f, 0.6f));
    SceneNode *ball6RotationNode = ball6Node->createChildSceneNode("Ball6RotationNode");
    SceneNode *ball6CenterNode = ball6RotationNode->createChildSceneNode("Ball6CenterNode", Vector3(25.74f, -4.27f, -20.6f));
    ball6CenterNode->attachObject(ball6);
    ball6Node->setVisible(false);

    SceneNode *ball7Node = mgr->getRootSceneNode()->createChildSceneNode("Ball7Node", Vector3(0.0f, 0.0f, 0.6f));
    SceneNode *ball7RotationNode = ball7Node->createChildSceneNode("Ball7RotationNode");
    SceneNode *ball7CenterNode = ball7RotationNode->createChildSceneNode("Ball7CenterNode", Vector3(23.83f, 0.58f, -20.6f));
    ball7CenterNode->attachObject(ball7);
    ball7Node->setVisible(false);

    SceneNode *ball8Node = mgr->getRootSceneNode()->createChildSceneNode("Ball8Node", Vector3(0.0f, 0.0f, 0.6f));
    SceneNode *ball8RotationNode = ball8Node->createChildSceneNode("Ball8RotationNode");
    SceneNode *ball8CenterNode = ball8RotationNode->createChildSceneNode("Ball8CenterNode", Vector3(22.15f, -2.36f, -20.6f));
    ball8CenterNode->attachObject(ball8);
    ball8Node->setVisible(false);

    SceneNode *ball9Node = mgr->getRootSceneNode()->createChildSceneNode("Ball9Node", Vector3(0.0f, 0.0f, 0.6f));
    SceneNode *ball9RotationNode = ball9Node->createChildSceneNode("Ball9RotationNode");
    SceneNode *ball9CenterNode = ball9RotationNode->createChildSceneNode("Ball9CenterNode", Vector3(25.75f, -2.33f, -20.6f));
    ball9CenterNode->attachObject(ball9);
    ball9Node->setVisible(false);

    SceneNode *ball10Node = mgr->getRootSceneNode()->createChildSceneNode("Ball10Node", Vector3(0.0f, 0.0f, 0.6f));
    SceneNode *ball10RotationNode = ball10Node->createChildSceneNode("Ball10RotationNode");
    SceneNode *ball10CenterNode = ball10RotationNode->createChildSceneNode("Ball10CenterNode", Vector3(23.9f, -1.38f, -20.6f));
    ball10CenterNode->attachObject(ball10);
    ball10Node->setVisible(false);

    SceneNode *ball11Node = mgr->getRootSceneNode()->createChildSceneNode("Ball11Node", Vector3(0.0f, 0.0f, 0.6f));
    SceneNode *ball11RotationNode = ball11Node->createChildSceneNode("Ball11RotationNode");
    SceneNode *ball11CenterNode = ball11RotationNode->createChildSceneNode("Ball11CenterNode", Vector3(22.12f, -0.4f, -20.6f));
    ball11CenterNode->attachObject(ball11);
    ball11Node->setVisible(false);

    SceneNode *ball12Node = mgr->getRootSceneNode()->createChildSceneNode("Ball12Node", Vector3(0.0f, 0.0f, 0.6f));
    SceneNode *ball12RotationNode = ball12Node->createChildSceneNode("Ball12RotationNode");
    SceneNode *ball12CenterNode = ball12RotationNode->createChildSceneNode("Ball12CenterNode", Vector3(25.59f, -6.18f, -20.6f));
    ball12CenterNode->attachObject(ball12);
    ball12Node->setVisible(false);

    SceneNode *ball13Node = mgr->getRootSceneNode()->createChildSceneNode("Ball13Node", Vector3(0.0f, 0.0f, 0.6f));
    SceneNode *ball13RotationNode = ball13Node->createChildSceneNode("Ball13RotationNode");
    SceneNode *ball13CenterNode = ball13RotationNode->createChildSceneNode("Ball13CenterNode", Vector3(25.66f, 1.61f, -20.6f));
    ball13CenterNode->attachObject(ball13);
    ball13Node->setVisible(false);

    SceneNode *ball14Node = mgr->getRootSceneNode()->createChildSceneNode("Ball14Node", Vector3(0.0f, 0.0f, 0.6f));
    SceneNode *ball14RotationNode = ball14Node->createChildSceneNode("Ball14RotationNode");
    SceneNode *ball14CenterNode = ball14RotationNode->createChildSceneNode("Ball14CenterNode", Vector3(23.86f, -3.42f, -20.6f));
    ball14CenterNode->attachObject(ball14);
    ball14Node->setVisible(false);

    SceneNode *ball15Node = mgr->getRootSceneNode()->createChildSceneNode("Ball15Node", Vector3(0.0f, 0.0f, 0.6f));
    SceneNode *ball15RotationNode = ball15Node->createChildSceneNode("Ball15RotationNode");
    SceneNode *ball15CenterNode = ball15RotationNode->createChildSceneNode("Ball15CenterNode", Vector3(20.37f, -3.19f, -20.6f));
    ball15CenterNode->attachObject(ball15);
    ball15Node->setVisible(false);

    Light* light = mgr->createLight("Light1");
    light->setType(Light::LT_SPOTLIGHT);
    light->setDiffuseColour(ColourValue(0.25f,0.25f,0.0f));
    light->setSpecularColour(ColourValue(0.25f,0.25f,0.0f));
    light->setSpotlightRange(Degree(60), Degree(70));
    light->setDirection(Vector3::NEGATIVE_UNIT_Z);
    SceneNode *lightNode = mgr->getRootSceneNode()->createChildSceneNode("LightNode1", Vector3(-20.0f, 0.0f, 30.0f));
    lightNode->attachObject(light);
    light->setVisible(false);

    light = mgr->createLight("Light2");
    light->setType(Light::LT_SPOTLIGHT);
    light->setDiffuseColour(ColourValue(0.25f,0.25f,0.0f));
    light->setSpecularColour(ColourValue(0.25f,0.25f,0.0f));
    light->setSpotlightRange(Degree(60), Degree(70));
    light->setDirection(Vector3::NEGATIVE_UNIT_Z);
    lightNode = mgr->getRootSceneNode()->createChildSceneNode("LightNode2", Vector3(0.0f, 0.0f, 30.0f));
    lightNode->attachObject(light);
    light->setVisible(false);

    light = mgr->createLight("Light3");
    light->setType(Light::LT_SPOTLIGHT);
    light->setDiffuseColour(ColourValue(0.25f,0.25f,0.0f));
    light->setSpecularColour(ColourValue(0.25f,0.25f,0.0f));
    light->setSpotlightRange(Degree(60), Degree(70));
    light->setDirection(Vector3::NEGATIVE_UNIT_Z);
    lightNode = mgr->getRootSceneNode()->createChildSceneNode("LightNode3", Vector3(20.0f, 0.0f, 30.0f));
    lightNode->attachObject(light);
    light->setVisible(false);
}

/**
 * This function initializes the OIS library (initializes mouse and keyboard).
 */
void Application::setupInputSystem() {
    size_t windowHnd = 0;
    std::ostringstream windowHndStr;
    OIS::ParamList pl;
    RenderWindow *win = mRoot->getAutoCreatedWindow();

    win->getCustomAttribute("WINDOW", &windowHnd);
    windowHndStr << windowHnd;
    pl.insert(std::make_pair(std::string("WINDOW"), windowHndStr.str()));
    mInputManager = OIS::InputManager::createInputSystem(pl);

    try {
        mKeyboard = static_cast<OIS::Keyboard*>(mInputManager->createInputObject(OIS::OISKeyboard, true));
        mMouse = static_cast<OIS::Mouse*>(mInputManager->createInputObject(OIS::OISMouse, true));

        const OIS::MouseState &ms = mMouse->getMouseState();
        ms.width = win->getWidth()/2;
        ms.height = win->getHeight()/2;
    }
    catch (const OIS::Exception &e) {
        throw Exception(42, e.eText, "Application::setupInputSystem");
    }
}

/**
 * This method initializes Crazy Edie User Interface (CEGUI). It loads
 * configuration files (with widget look and feel, definition of menus,
 * default font) and draw main menu on screen
 */
void Application::setupCEGUI() {
    SceneManager *mgr = mRoot->getSceneManager("SceneManager");
    RenderWindow *win = mRoot->getAutoCreatedWindow();
    mContinue = true;

    mRenderer = new CEGUI::OgreCEGUIRenderer(win, Ogre::RENDER_QUEUE_OVERLAY, false, 3000, mgr);
    mSystem = new CEGUI::System(mRenderer);
    mWindowManager = CEGUI::WindowManager::getSingletonPtr();

    CEGUI::Logger::getSingleton().setLoggingLevel (CEGUI::Informative);
    CEGUI::SchemeManager::getSingleton().loadScheme("psg.scheme");
    CEGUI::FontManager::getSingleton().createFont("DejaVuSans-10.font");
    mSystem->setDefaultFont((CEGUI::utf8*) "DejaVuSans-10");

    mSystem->setDefaultMouseCursor((CEGUI::utf8*) "PSG",
                                   (CEGUI::utf8*) "MouseArrow");
    
    
    mUI = new UI(mRoot, mRenderer, mSystem, mWindowManager, &mContinue, game);
    mUI->setScene(M_MAIN_MENU); 

}

/**
 * This method creates frame listeners. Frame listeners are executed everytime the frame is to be rendered (or after rendering).
 * It defines frame listener for game and menus.
 */
void Application::createFrameListener() {
    mMenuListener = new MenuFrameListener (mKeyboard, 
        mMouse,
        mRoot->getSceneManager("SceneManager"),
        mSystem,&mContinue,game);
    mListener = new GameListener(mKeyboard, mMouse, 
                                 mRoot->getSceneManager("SceneManager"), 
                                 game, mUI, mMenuListener);

    mRoot->addFrameListener(mListener);
    mRoot->addFrameListener(mMenuListener);

}

/**
 * This function says OGRE that it should start rendering now.
 */
void Application::startRenderLoop() {
    mRoot->startRendering();
}

/**
 * This is the main function. It executes Application::go function.
 */
int main(int argc, char **argv)
{
    Application app;

    try {
        app.go();
    } catch(Ogre::Exception &e) {
        fprintf(stderr, "Failed to initialize application: %s\n", e.what());
    }

    return 0;
}
