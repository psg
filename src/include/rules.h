/*
 * Copyright 2008 Jacek Caban
 * Copyright 2008 Piotr Caban
 * Copyright 2008 Jaroslaw Sobiecki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RULES_H
#define RULES_H

#include <Ogre.h>

#include "physics.h"

/**
 * This class keeps the state of game. Says what is against rules and what is acceptable.
 */
class PoolGame {
    private:
        bool stopped;
        int player;
        int color;
        int faul;
        Table *game;
        void init(void);
        void destroy(void);
        bool pocketedBalls[16];
        Ogre::Real sizeX;
        Ogre::Real sizeY;

    public:
        PoolGame(Ogre::Real sizeX = 60.0, Ogre::Real sizeY = 30.0);
        ~PoolGame();
        bool isWaitingForShot(Ogre::Real);
        bool isWhiteBallMovable(void);
        int currentPlayer(void);
        int currentColor(void);
        int gameEnded(void);
        Ogre::Vector3 getBallPosition(int);
        Ogre::Quaternion getBallOrientation(int);
        bool isBallOnTable(int);
        void shot(Ogre::Real, Ogre::Real, Ogre::Real, Ogre::Real, Ogre::Real);
        void restart(void);
        void setWhitePosition(Ogre::Vector3 &pos);
};

#endif
