/*
 * Copyright 2008 Jacek Caban
 * Copyright 2008 Piotr Caban
 * Copyright 2008 Jaroslaw Sobiecki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
#include <Ogre.h>
#include <OIS/OIS.h>
#include <CEGUI/CEGUI.h>
#include <OgreCEGUIRenderer.h>
#include "rules.h"

#ifndef _UI_H
#define _UI_H


/**
 * This enum represents which menu is currently enabled.
 * We need those states in order to choose correct event handlers in
 * our framelisteners
 */
enum Scene {
    M_MAIN_MENU,
    M_OPTIONS_MENU,
    M_HUD_MENU,
    M_IN_GAME_MENU,
    M_SET_SHOT_POWER,
    M_DISABLED
};

/**
 *  This class is one of our framelisteners. It's responsible
 *  for sending mouse events from OIS library to CEGUI library
 *  and for updating our HUD messages
 */
class MenuFrameListener : public Ogre::FrameListener, 
                          public OIS::MouseListener
{
  public:
    MenuFrameListener(OIS::Keyboard *keyboard, OIS::Mouse *mouse, Ogre::SceneManager *mgr,
                      CEGUI::System *system, bool *cont, PoolGame *game);
    bool frameStarted(const Ogre::FrameEvent &evt);

    bool mouseMoved(const OIS::MouseEvent &arg);
    bool mousePressed(const OIS::MouseEvent &arg, OIS::MouseButtonID id);
    bool mouseReleased(const OIS::MouseEvent &arg, OIS::MouseButtonID id);
    bool keyPressed(const OIS::KeyEvent &e); 
    bool keyReleased(const OIS::KeyEvent &e); 
    CEGUI::MouseButton static convertButton(OIS::MouseButtonID buttonID);
    

  private:
    OIS::Keyboard *mKeyboard;
    OIS::Mouse *mMouse;
    CEGUI::System *mSystem;
    bool *mContinue;
    PoolGame *mGame;
    int last_player;
    int last_color;
};

/**
 *
 * This class implements our 2D CEGUI interface
 * It's responsible for changing current menu,
 * setting power of ball shot and assigning event 
 * handler to various CEGUI events
 */
class UI {
  public:
    UI (Ogre::Root *&root, 
        CEGUI::OgreCEGUIRenderer *mRenderer, 
        CEGUI::System *mSystem, 
        CEGUI::WindowManager *wmgr,
        bool *cont,
        PoolGame *game);
    bool setScene (Scene sc);
    Scene getScene ();
    Ogre::Real getPowerShot();
  
    //These are simple events handlers
  private:
    //private helper functions
    void setEventHandlers (Scene sc);
    void add_quit_event_handler(const CEGUI::String window_name);
    void add_start_event_handler(const CEGUI::String window_name);
    void add_restart_event_handler(const CEGUI::String window_name);
    void add_continue_event_handler(const CEGUI::String window_name);
    void add_slider_event_hander(const CEGUI::String window_name);


    //private 
    bool quitButtonHandler (const CEGUI::EventArgs &event);
    bool sliderButtonHandler(const CEGUI::EventArgs &event);
    bool startButtonHandler (const CEGUI::EventArgs &event);
    bool restartButtonHandler (const CEGUI::EventArgs &event);
    bool continueButtonHandler (const CEGUI::EventArgs &event);


    //private members
    
    //! This member is unused and should be removed
    CEGUI::OgreCEGUIRenderer *mRenderer;
    //! This member is used to switch currently loaded menu.
    CEGUI::System *mSystem;
    //! This member is used to load Window (widgets) instances from .layout file
    CEGUI::WindowManager *mWindowManager;
    //! It's and statring point for ogre application. We can gain access to all subsystems of ogre library from there
    Ogre::Root *&mRoot; 
   
   //! This is enum value, which means currently chossen scene 
    Scene mCurrentScene;
    //! We return this value in our FrameListener class. If it's point to 0 value, game stops
    bool *mContinue;

    //! This is loaded layout from main_menu.layout file. It's consist all button from main menu
    CEGUI::Window *main_sheet;
    //! This is loaded layout from game_hud.layout file. It's consist all widgets from HUD
    CEGUI::Window *hud_sheet;
    //! This is loaded layout from in_game_menu.layout file. It's consist all widgets from game menu
    CEGUI::Window *in_game_sheet;
    //! This is pointer, witch points to PoolGame class. 
    PoolGame *mGame;

    Ogre::Real mPowerShoot;

};

#endif
