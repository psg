/*
 * Copyright 2008 Jacek Caban
 * Copyright 2008 Piotr Caban
 * Copyright 2008 Jaroslaw Sobiecki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PHYSICS_H
#define PHYSICS_H

#include <vector>

/**
 * Ball class represents state of a ball.
 */
class Ball {
private:
    Ogre::Vector3 position;
    Ogre::Vector3 speed;
    Ogre::Vector3 rotation;
    Ogre::Quaternion orientation;
    Ogre::Real r;
    Ogre::Real mass;
    bool onTable;
    int firstHit;
    int ballNo;

    friend class Table;

public:
    Ball(int ballNo, Ogre::Vector3 position,
         Ogre::Vector3 speed = Ogre::Vector3::ZERO,
         Ogre::Vector3 rotation = Ogre::Vector3::ZERO,
         Ogre::Real r = 1.0,
         Ogre::Real m = 0.5)
        : ballNo(ballNo), position(position), speed(speed), rotation(rotation), r(r), mass(m), onTable(true), firstHit(false)
    {}

    void shot(Ogre::Real direction, Ogre::Real force, Ogre::Vector3 shotPos,
              Ogre::Real forceScale = 140.6, Ogre::Real hitTime = 0.1);

    Ogre::Vector3 &getPosition() {
        return position;
    }

    Ogre::Quaternion &getOrientation() {
        return orientation;
    }

    bool &isOnTable() {
        return onTable;
    }

    int &getFirstHit() {
        return firstHit;
    }

    Ogre::Real getR() {
        return r;
    }

    int getID() {
        return ballNo;
    }
};

/**
 * TableParameters class is a collection of physics parameters.
 */
class TableParameters {
public:
    Ogre::Real staticFriction;
    Ogre::Real slidingFriction;
    Ogre::Real ballsFriction;
    Ogre::Real boundFriction;
    Ogre::Real ballZFriction;
    Ogre::Real gravity;
    Ogre::Real boundResistance;
    Ogre::Real pocketSize;

    TableParameters() :
        staticFriction(0.99),
        slidingFriction(0.03),
        ballsFriction(0.1),
        boundFriction(0.3),
        ballZFriction(0.5),
        gravity(9.81),
        boundResistance(0.9),
        pocketSize(6.0)
    {}

    static const TableParameters DefaultTableParameters;
};

/**
 * The Table class represenst state of the table and implements all its physics aspects.
 */
class Table {
private:
    const Ogre::Real size_x, size_y;
    struct timeval start_time;
    Ogre::Real current_time;
    const TableParameters *params;

    Ogre::Real eulerUpdate();
    void simpleUpdate(Ogre::Real dt);

    Ogre::Real boundCrossTime(Ball &ball, int &bound);
    Ogre::Real ballCrossTime(Ball &ball1, Ball &ball2);
    void boundCross(Ball &ball, int bound);
    void ballCross(Ball &ball, Ball &ball2);
    bool ballsMoving();
    inline void ballFallIn(Ball &ball);

public:
    typedef std::vector<Ball*> BallVector;
    BallVector balls;

    Table(Ogre::Real sx, Ogre::Real sy, const TableParameters *p = &TableParameters::DefaultTableParameters);
    bool update(Ogre::Real time);
};

#endif

