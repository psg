
SUBDIRS = src bin doc

bin/psg: src_compiled
	cp src/psg bin/psg

src_compiled:
	@cd src && $(MAKE) psg

all: bin/psg

generate_doc: bin/psg 
	doxygen

doc-pdf: generate_doc
	@cd doc/latex && $(MAKE) all && cp refman.pdf ../refman.pdf

$(SUBDIRS:%=%/__clean__):
	@cd `dirname $@` && $(MAKE) clean

clean: $(SUBDIRS:%=%/__clean__)
	rm -f psg-*.tar.bz2

run: bin/psg
	cd bin && ./psg

distclean: clean

snapshot: distclean
	tar -cjf psg-$(cat VERSION).tar.bz2 *
